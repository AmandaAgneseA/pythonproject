INSERT INTO user (username, password) VALUES ('amanda', 'amanda');

INSERT INTO project (title, user_id) VALUES ('projekts_1', (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO project (title, user_id) VALUES ('projekts_2', (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO project (title, user_id) VALUES ('projekts_3', (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO project (title, user_id) VALUES ('projekts_4', (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO project (title, user_id) VALUES ('projekts_5', (SELECT id FROM user WHERE username = 'amanda'));

INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_1_1', (SELECT id FROM project WHERE title = 'projekts_1'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_1_2', (SELECT id FROM project WHERE title = 'projekts_1'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_1_3', (SELECT id FROM project WHERE title = 'projekts_1'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_1_4', (SELECT id FROM project WHERE title = 'projekts_1'), (SELECT id FROM user WHERE username = 'amanda'));

INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_2_1', (SELECT id FROM project WHERE title = 'projekts_2'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_2_2', (SELECT id FROM project WHERE title = 'projekts_2'), (SELECT id FROM user WHERE username = 'amanda'));

INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_3_1', (SELECT id FROM project WHERE title = 'projekts_3'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_3_2', (SELECT id FROM project WHERE title = 'projekts_3'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_3_3', (SELECT id FROM project WHERE title = 'projekts_3'), (SELECT id FROM user WHERE username = 'amanda'));

INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_4_1', (SELECT id FROM project WHERE title = 'projekts_4'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_4_2', (SELECT id FROM project WHERE title = 'projekts_4'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_4_3', (SELECT id FROM project WHERE title = 'projekts_4'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_4_4', (SELECT id FROM project WHERE title = 'projekts_4'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_4_5', (SELECT id FROM project WHERE title = 'projekts_4'), (SELECT id FROM user WHERE username = 'amanda'));

INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_5_1', (SELECT id FROM project WHERE title = 'projekts_5'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_5_2', (SELECT id FROM project WHERE title = 'projekts_5'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_5_3', (SELECT id FROM project WHERE title = 'projekts_5'), (SELECT id FROM user WHERE username = 'amanda'));
INSERT INTO task (title, project_id, user_id) VALUES ('uzdevums_5_4', (SELECT id FROM project WHERE title = 'projekts_5'), (SELECT id FROM user WHERE username = 'amanda'));

INSERT INTO planning (planned_time, task_id) VALUES ('30', (SELECT id FROM task WHERE title = 'uzdevums_1_1'));
INSERT INTO planning (planned_time, task_id) VALUES ('40', (SELECT id FROM task WHERE title = 'uzdevums_1_2'));
INSERT INTO planning (planned_time, task_id) VALUES ('60', (SELECT id FROM task WHERE title = 'uzdevums_1_3'));
INSERT INTO planning (planned_time, task_id) VALUES ('90', (SELECT id FROM task WHERE title = 'uzdevums_1_4'));
INSERT INTO planning (planned_time, task_id) VALUES ('40', (SELECT id FROM task WHERE title = 'uzdevums_2_1'));
INSERT INTO planning (planned_time, task_id) VALUES ('70', (SELECT id FROM task WHERE title = 'uzdevums_2_2'));
INSERT INTO planning (planned_time, task_id) VALUES ('120', (SELECT id FROM task WHERE title = 'uzdevums_3_1'));
INSERT INTO planning (planned_time, task_id) VALUES ('160', (SELECT id FROM task WHERE title = 'uzdevums_3_2'));
INSERT INTO planning (planned_time, task_id) VALUES ('30', (SELECT id FROM task WHERE title = 'uzdevums_3_3'));
INSERT INTO planning (planned_time, task_id) VALUES ('20', (SELECT id FROM task WHERE title = 'uzdevums_4_1'));
INSERT INTO planning (planned_time, task_id) VALUES ('100', (SELECT id FROM task WHERE title = 'uzdevums_4_2'));
INSERT INTO planning (planned_time, task_id) VALUES ('360', (SELECT id FROM task WHERE title = 'uzdevums_4_3'));
INSERT INTO planning (planned_time, task_id) VALUES ('80', (SELECT id FROM task WHERE title = 'uzdevums_4_4'));
INSERT INTO planning (planned_time, task_id) VALUES ('400', (SELECT id FROM task WHERE title = 'uzdevums_4_5'));
INSERT INTO planning (planned_time, task_id) VALUES ('500', (SELECT id FROM task WHERE title = 'uzdevums_5_1'));
INSERT INTO planning (planned_time, task_id) VALUES ('60', (SELECT id FROM task WHERE title = 'uzdevums_5_2'));
INSERT INTO planning (planned_time, task_id) VALUES ('120', (SELECT id FROM task WHERE title = 'uzdevums_5_3'));
INSERT INTO planning (planned_time, task_id) VALUES ('180', (SELECT id FROM task WHERE title = 'uzdevums_5_4'));

INSERT INTO actual (actual_time, task_id) VALUES ('40', (SELECT id FROM task WHERE title = 'uzdevums_1_1'));
INSERT INTO actual (actual_time, task_id) VALUES ('30', (SELECT id FROM task WHERE title = 'uzdevums_1_2'));
INSERT INTO actual (actual_time, task_id) VALUES ('40', (SELECT id FROM task WHERE title = 'uzdevums_1_3'));
INSERT INTO actual (actual_time, task_id) VALUES ('90', (SELECT id FROM task WHERE title = 'uzdevums_1_4'));
INSERT INTO actual (actual_time, task_id) VALUES ('40', (SELECT id FROM task WHERE title = 'uzdevums_2_1'));
INSERT INTO actual (actual_time, task_id) VALUES ('90', (SELECT id FROM task WHERE title = 'uzdevums_2_2'));
INSERT INTO actual (actual_time, task_id) VALUES ('180', (SELECT id FROM task WHERE title = 'uzdevums_3_1'));
INSERT INTO actual (actual_time, task_id) VALUES ('2000', (SELECT id FROM task WHERE title = 'uzdevums_3_2'));
INSERT INTO actual (actual_time, task_id) VALUES ('30', (SELECT id FROM task WHERE title = 'uzdevums_3_3'));
INSERT INTO actual (actual_time, task_id) VALUES ('20', (SELECT id FROM task WHERE title = 'uzdevums_4_1'));
INSERT INTO actual (actual_time, task_id) VALUES ('800', (SELECT id FROM task WHERE title = 'uzdevums_4_2'));
INSERT INTO actual (actual_time, task_id) VALUES ('300', (SELECT id FROM task WHERE title = 'uzdevums_4_3'));
INSERT INTO actual (actual_time, task_id) VALUES ('80', (SELECT id FROM task WHERE title = 'uzdevums_4_4'));
INSERT INTO actual (actual_time, task_id) VALUES ('300', (SELECT id FROM task WHERE title = 'uzdevums_4_5'));
INSERT INTO actual (actual_time, task_id) VALUES ('450', (SELECT id FROM task WHERE title = 'uzdevums_5_1'));
INSERT INTO actual (actual_time, task_id) VALUES ('30', (SELECT id FROM task WHERE title = 'uzdevums_5_2'));
INSERT INTO actual (actual_time, task_id) VALUES ('180', (SELECT id FROM task WHERE title = 'uzdevums_5_3'));
INSERT INTO actual (actual_time, task_id) VALUES ('180', (SELECT id FROM task WHERE title = 'uzdevums_5_4'));

-- SELECT planned_time, actual_time
-- FROM planning
-- JOIN actual ON planning.task_id = actual.task_id
-- WHERE
