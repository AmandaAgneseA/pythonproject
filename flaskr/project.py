from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

import matplotlib.pyplot as plt
import numpy as np
import io
from io import BytesIO
import base64

bp = Blueprint('project', __name__)

@bp.route('/')
def index():
    db = get_db()
    projects = db.execute(
        'SELECT p.id, title, priority, notes, created, user_id'
        ' FROM project p JOIN user u ON p.user_id = u.id'
        ' ORDER BY priority ASC'
    ).fetchall()

    return render_template('project/index.html', projects=projects)

@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        priority = request.form['priority']
        notes = request.form['notes']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO project (title, priority, notes, user_id)'
                ' VALUES (?, ?, ?, ?)',
                (title, priority, notes, g.user['id'])
            )
            db.commit()
            return redirect(url_for('project.index'))

    return render_template('project/create.html')

def get_project(id, check_author=True):
    project = get_db().execute(
        'SELECT p.id, title, priority, notes, created, user_id, username'
        ' FROM project p'
        ' JOIN user u ON p.user_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if project is None:
        abort(404, "Project id {0} doesn't exist.".format(id))

    if check_author and project['user_id'] != g.user['id']:
        abort(403)

    return project

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    project = get_project(id)
    task = None

    if request.method == 'POST':
        title = request.form['title']
        priority = request.form['priority']
        notes = request.form['notes']

        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE project SET title = ?, priority = ?, notes = ?'
                ' WHERE id = ?',
                (title, priority, notes, id)
            )

            db.commit()
            return redirect(url_for('project.index'))

    return render_template('project/update.html', project=project)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_project(id)
    db = get_db()
    db.execute('DELETE FROM project WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('project.index'))

@bp.route('/graphs')
def graphs():
    db = get_db()
    projectID = 'projekts_1'
    dbdata = db.execute(
        'SELECT planned_time, actual_time, task.title'
        ' FROM planning'
        ' JOIN actual ON planning.task_id = actual.task_id'
        ' JOIN task ON planning.task_id = task.id'
        ' WHERE planning.task_id IN ('
            ' SELECT task.id'
            ' FROM task'
            ' JOIN project ON task.project_id = project.id'
            ' WHERE project.title = ? )', (projectID,)
    ).fetchall()

    planned = list()
    actual = list()
    for row in dbdata:
        planned.append('{}'.format(row["planned_time"]))
        actual.append('{}'.format(row["actual_time"]))
    planned = list(map(int, planned))
    actual = list(map(int, actual))
    print(planned)
    print(actual)

    # ind = np.arange(len(planned))  # the x locations for the groups
    # width = 0.35  # the width of the bars
    #
    # men_std = (2, 3, 4, 1, 2)
    # women_std = (3, 5, 2, 3, 3)
    #
    # fig, ax = plt.subplots()
    # rects1 = ax.bar(ind - width/2, planned, width, yerr=men_std,
    #                 color='SkyBlue', label='planned')
    # rects2 = ax.bar(ind + width/2, actual, width, yerr=women_std,
    #                 color='IndianRed', label='actual')
    #
    # ax.set_ylabel('Scores')
    # ax.set_title('Scores by group and gender')
    # ax.set_xticks(ind)
    # ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
    # ax.legend()
    #
    # img1 = BytesIO()
    #
    # def autolabel(rects, xpos='center'):
    #     """
    #     Attach a text label above each bar in *rects*, displaying its height.
    #
    #     *xpos* indicates which side to place the text w.r.t. the center of
    #     the bar. It can be one of the following {'center', 'right', 'left'}.
    #     """
    #
    #     xpos = xpos.lower()  # normalize the case of the parameter
    #     ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    #     offset = {'center': 0.5, 'right': 0.57, 'left': 0.43}  # x_txt = x + w*off
    #
    #     for rect in rects:
    #         height = rect.get_height()
    #         ax.text(rect.get_x() + rect.get_width()*offset[xpos], 1.01*height,
    #                 '{}'.format(height), ha=ha[xpos], va='bottom')
    #
    #     plt.savefig(img1, format='png')
    #
    #
    # autolabel(rects1, "left")
    # autolabel(rects2, "right")

# plt.show()

    # x1 = ['0', '1', '2', '3', '4', '8']
    # y1 = ['10', '30', '40', '5', '50', '90']




    x1 = [0, 1, 2, 3, 4, 8]
    y1 = [10, 30, 45, 5, 50, 90]
    img1 = BytesIO()
    # graph1_url = build_graph(x1,y1);
    plt.plot(x1,y1)
    plt.savefig(img1, format='png')
    img1.seek(0)
    plot_url1 = base64.b64encode(img1.getvalue()).decode()

    x2 = [0, 1, 2, 3, 4]
    y2 = [50, 30, 20, 10, 50]
    img2 = BytesIO()
    # graph2_url = build_graph(x2,y2);

    plt.plot(x2,y2)
    plt.savefig(img2, format='png')
    img2.seek(0)
    plot_url2 = base64.b64encode(img2.getvalue()).decode()

    x3 = [0, 1, 2, 3, 4]
    y3 = [0, 30, 10, 5, 30]
    img3 = BytesIO()
    # graph3_url = build_graph(x3,y3);
    plt.plot(x3,y3)
    plt.savefig(img3, format='png')
    img3.seek(0)
    plot_url3 = base64.b64encode(img3.getvalue()).decode()


    # # x = np.linspace(0, 2, 100)



    #
    # return '<img src="data:image/png;base64,{}">'.format(plot_url)


    return render_template('project/graphs.html',
    graph1=plot_url1,
    graph2=plot_url2,
    graph3=plot_url3,
    dbdata=dbdata)

if __name__ == '__main__':
    app.debug = True
    app.run()
