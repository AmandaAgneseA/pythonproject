from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

import datetime
#https://www.saltycrane.com/blog/2008/06/how-to-get-current-date-and-time-in/


bp = Blueprint('task', __name__, url_prefix='/tasks')

@bp.route('/')
def index():
    db = get_db()
    tasks = db.execute(
        'SELECT t.id, title, notes, priority, place, created, user_id'
        ' FROM task t JOIN user u ON t.user_id = u.id'
        ' ORDER BY priority ASC'
    ).fetchall()

    return render_template('task/index.html', tasks=tasks)

@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    db = get_db()
    projects = db.execute(
        'SELECT p.id, title, priority, user_id'
        ' FROM project p JOIN user u ON p.user_id = u.id'
        ' ORDER BY priority ASC'
    ).fetchall()

    if request.method == 'POST':
        project_id = request.form['project_id']
        title = request.form['title']
        priority = request.form['priority']
        notes = request.form['notes']
        place = request.form['place']

        start_date = request.form['start-date']
        end_date = request.form['end-date']
        planned_time = request.form['planned-time']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            task_id = db.execute(
                'INSERT INTO task (title, priority, notes, place, user_id, project_id)'
                ' VALUES (?, ?, ?, ?, ?, ?)',
                (title, priority, notes, place, g.user['id'], project_id)
            ).lastrowid

            if start_date is not None:
                db.execute(
                    'INSERT INTO planning (start_date, end_date, planned_time, task_id)'
                    ' VALUES (?, ?, ?, ?)',
                    (start_date, end_date, planned_time, task_id)
                )

            db.commit()
            return redirect(url_for('task.index'))

    return render_template('task/create.html', projects=projects)

def get_task(id, check_author=True):
    task = get_db().execute(
        'SELECT t.id, project_id, title, priority, notes, place, created, user_id, username'
        ' FROM task t JOIN user u ON t.user_id = u.id'
        ' WHERE t.id = ?',
        (id,)
    ).fetchone()

    if task is None:
        abort(404, "Project id {0} doesn't exist.".format(id))

    if check_author and task['user_id'] != g.user['id']:
        abort(403)

    return task

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    task = get_task(id)
    db = get_db()
    projects = db.execute(
        'SELECT p.id, title, priority, user_id'
        ' FROM project p JOIN user u ON p.user_id = u.id'
        ' ORDER BY priority ASC'
    ).fetchall()

    if request.method == 'POST':
        project_id = request.form['project_id']
        title = request.form['title']
        priority = request.form['priority']
        notes = request.form['notes']
        place = request.form['place']

        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db.execute(
                'UPDATE task SET project_id = ?, title = ?, priority = ?, notes = ?, place = ?'
                ' WHERE id = ?',
                (project_id, title, priority, notes, place, id)
            )

            db.commit()
            return redirect(url_for('task.index'))

    return render_template('task/update.html', projects=projects, task=task)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_task(id)
    db = get_db()
    db.execute('DELETE FROM task WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('task.index'))

if __name__ == '__main__':
    app.debug = True
    app.run()
