CREATE TABLE project (
	id INT IDENTITY(1,1) PRIMARY KEY,
	title NVARCHAR(255) NULL
);
CREATE TABLE task (
	id INTEGER IDENTITY(1,1) PRIMARY KEY,
	title NVARCHAR(255) NULL,
	project_id INTEGER NOT NULL,
    FOREIGN KEY (project_id) REFERENCES project (id),
);
CREATE TABLE planning (
	planned_time INTEGER NULL,
    task_id INTEGER NOT NULL,
    FOREIGN KEY (task_id) REFERENCES task (id)
);
CREATE TABLE actual(
	actual_time INTEGER NULL,
    task_id INTEGER NOT NULL,
    FOREIGN KEY (task_id) REFERENCES task (id)
);